---
title: "Manufatura"
date: 2021-09-18T23:27:57-03:00
draft: false
---

A manufatura do formula é baseada em 3 vertentes:

&nbsp;

1. Tornearia: Utilização de um torno para a fabricação de peças em formato cilindrico , sendo exemplo: buchas , eixos e mancais.

&nbsp;

&nbsp;

&nbsp;

![](https://www.usibr.com.br/servicos-de-usinagem/imagens/orcamento-de-servico-de-usinagem-tornearia-e-solda.jpg)

&nbsp;

&nbsp;

&nbsp;

2. Fresamento: Utilização de uma fresadora para a fabricação de peças da mais diversas formas:

&nbsp;

&nbsp;

&nbsp;

![](https://www.blog.meuguru.net/wp-content/uploads/2022/12/milling-cutters-gc18a8a07e_640.jpg)

&nbsp;

&nbsp;

&nbsp;

3. CNC: Uitlização de um torno ou fresa CNC, grande game de produtos que podem ser fabricados utilizando eles e o mais preciso:

&nbsp;

&nbsp;

&nbsp;

![](https://www.usinagemfenix.com.br/media/sig_YATJTEOPLL/produtos/maquina-torno-cnc/maquina-torno-cnc-01.jpeg)


 